# To run this API you shoud have docker install on your pc 
## Use " docker-compose up -d "

## To signIn use this url localhost:3000/api/auth/signin
{
	"email":"admin@admin.com",
	"password":"123456"
}
## Endpoints
### Auth
POST localhost:3000/api/auth/signin
POST localhost:3000/api/auth/signup
### Products 
(GET, POST, PUT, DELETE) localhost:3000/api/products
### Users
(GET, POST, PUT, DELETE) localhost:3000/api/users