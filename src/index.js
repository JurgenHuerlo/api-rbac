import app from "./app";
import db from "./database";

app.listen(3000);

console.log("Server listening on port", 3000);
console.log("click on: http://localhost:3000");