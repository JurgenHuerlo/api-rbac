import Role from "../models/role";
import User from "../models/user";

//Create all roles
export const createRoles = async () => {
  try {
    const count = await Role.estimatedDocumentCount();

    if (count > 0) return;

    const value = await Promise.all([
      new Role({ name: "user" }).save(),
      new Role({ name: "moderator" }).save(),
      new Role({ name: "admin" }).save(),
    ]);
    createAdminUser();
  } catch (error) {
    console.error(error);
  }
};

//Create a Admin user the first time the API is run
const createAdminUser = async () => {
  try {
    //const count = await Role.estimatedDocumentCount();
    //const count2 = await User.estimatedDocumentCount();

    const idAdminRole = await Role.find({ name: "admin" });
    const idModeratorRole = await Role.find({ name: "moderator" });

    const value = await Promise.all([
      new User({
        username: "admin",
        email: "admin@admin.com",
        password: await User.encryptPassword("123456"),
        roles: [idAdminRole[0]._id, idModeratorRole[0]._id],
      }).save(),
    ]);

    // if (count > 0 && count2 === 0) {

    // } else return;
  } catch (error) {
    console.error(error);
  }
};
