import User from "../models/user";
import Role from "../models/role";

export const GetUsers = async (req, res) => {
  const users = await User.find();
  res.status(200).json(users);
};

export const GetUser = async (req, res) => {
  const user = await User.findById({ _id: req.params.userId });
  res.status(200).json(user);
};

export const createUser = async (req, res) => {

  const { username, email, password, roles } = req.body;

  const newUser = new User({
    username,
    email,
    password: await User.encryptPassword(password),
  });

  if (roles) {
    const foundRoles = await Role.find({ name: { $in: req.body.roles } });
    newUser.roles = foundRoles.map((role) => role._id);
  } else {
    const role = await Role.findOne({ name: "user" });
    newUser.roles = [role._id];
  }

  const savedUser = await newUser.save();

  res.status(200).json(savedUser);
};

export const updateUser = async (req, res) => {

  //TODO: Mejorar el como actualizar, la contraseña y los roles al mismo tiempo

  if (!req.body.password && !req.body.roles) {
    const updateUser = await User.findByIdAndUpdate(
      { _id: req.params.userId },
      req.body,
      { new: true }
    );
    return res.status(204).json(updateUser);
  }

  if (req.body.roles) {
    const foundRoles = await Role.find({ name: { $in: req.body.roles } });
    const userRole = foundRoles.map((role) => role._id);
    console.log(userRole);
    const updateUser = await User.findByIdAndUpdate(
      { _id: req.params.userId },
      { roles: userRole },
      { new: true }
    );
    return res.status(204).json(updateUser);
  }

  if (req.body.password) {
    const passHash = await User.encryptPassword(req.body.password);
    const updateUser = await User.findByIdAndUpdate(
      { _id: req.params.userId },
      { password: passHash },
      { new: true }
    );
    return res.status(204).json(updateUser);
  }
};

export const DeleteUser = async (req, res) => {
  const user = await User.findByIdAndDelete({ _id: req.params.userId });
  res.status(200).json(user);
};
