import User from "../models/user";
import jwt from "jsonwebtoken";
import config from "../config";
import Role from "../models/role";
import { validationResult } from "express-validator";

export const signUp = async (req, res) => {
  const { username, email, password, roles } = req.body;

  const newUser = new User({
    username,
    email,
    password: await User.encryptPassword(password),
  });

  if (!roles) {
    const role = await Role.findOne({ name: "user" });
    newUser.roles = [role._id];
  } else {
    return res.status(403).json({ message: "Only admin user can set roles" });
  }

  const savedUser = await newUser.save();

  //Expira cada 24 horas el token
  const token = jwt.sign({ id: savedUser._id }, config.SECRET, {
    expiresIn: 86400,
  });

  res.status(200).json(token);
};

export const signIn = async (req, res) => {
  
  //Show messages of Express validator
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ error: errors.array() });
  }

  const userFound = await User.findOne({ email: req.body.email }).populate(
    "roles"
  );
  if (!userFound) return res.status(404).json({ message: "User Not Found" });

  const matchPassword = await User.comparePassword(
    req.body.password,
    userFound.password
  );

  if (!matchPassword)
    return res.status(401).json({ token: null, message: "Invalid Password" });

  const token = jwt.sign({ id: userFound._id }, config.SECRET, {
    expiresIn: 86400,
  });

  res.status(200).json({ token: token });
};
