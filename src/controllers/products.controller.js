import Product from "../models/product";

export const createProduct = async (req, res) => {
  console.log(req.body);

  const newProduct = new Product({
    name: req.body.name,
    category: req.body.category,
    price: req.body.price,
    imgUrl: req.body.imgUrl,
  });

  const productSave = await newProduct.save();

  res.status(201).json(productSave);
};

export const getProducts = async (req, res) => {
  const products = await Product.find();
  res.status(200).json(products);
};
export const getProductById = async (req, res) => {
  const product = await Product.findById({ _id: req.params.productId });
  res.status(200).json(product);
};
export const updateProductById = async (req, res) => {
  const updateProduct = await Product.findByIdAndUpdate(
    { _id: req.params.productId },
    req.body,
    { new: true }
  );
  res.status(204).json(updateProduct);
};
export const deleteProductById = async (req, res) => {
  const deleteProduct = await Product.findByIdAndDelete({
    _id: req.params.productId,
  });
  res.status(200).json(deleteProduct);
};
