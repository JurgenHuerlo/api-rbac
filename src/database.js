import mongoose from "mongoose";

mongoose
  .connect("mongodb://mongodb/companydb", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    useCreateIndex: true,
    useFindAndModify:false
  })
  .then((db) => console.log("Db is connected"))
  .catch((error) => console.log(error));
