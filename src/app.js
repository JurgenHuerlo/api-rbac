import express from "express";
import morgan from "morgan";
import pkg from "../package.json";
import { createRoles} from "./libs/initialSetup";

//Routes
import ProducRoutes from "./routes/products.routes";
import AuthRoutes from "./routes/auth.routes";
import UserRoutes from "./routes/user.routes";

const app = express();

//Execute Seeders
createRoles();

//Middlewares
app.use(express.json());
app.use(morgan("dev"));

app.set("pkg", pkg);

//Routes
app.get("/", (req, res) => {
  res.json({
    name: app.get("pkg").name,
    author: app.get("pkg").author,
    description: app.get("pkg").description,
    version: app.get("pkg").version,
  });
});

app.use("/api/products", ProducRoutes);
app.use("/api/auth", AuthRoutes);
app.use("/api/users", UserRoutes);

export default app;
