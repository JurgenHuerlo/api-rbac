import { Router } from "express";
import {
  createProduct,
  deleteProductById,
  updateProductById,
  getProductById,
  getProducts,
} from "../controllers/products.controller";

import { authjwt } from "../middlewares";

const router = Router();

router.post("/", [authjwt.verifyToken, authjwt.isModerator], createProduct);
router.get("/", getProducts);
router.get("/:productId", getProductById);
router.put("/:productId",
  [authjwt.verifyToken, authjwt.isModerator],
  updateProductById
);
router.delete("/:productId",
  [authjwt.verifyToken, authjwt.isModerator],
  deleteProductById
);

export default router;