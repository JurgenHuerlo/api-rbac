import {
  createUser,
  GetUser,
  GetUsers,
  updateUser,
  DeleteUser,
} from "../controllers/user.controller";
import { authjwt } from "../middlewares";
import {
  checkRolesExist,
  checkDuplicateUsernameOrEmail,
} from "../middlewares/verifySignUp";

import { Router } from "express";
const router = Router();

router.get(
  "/",
  [authjwt.verifyToken, checkRolesExist, authjwt.isAdmin],
  GetUsers
);

router.get(
  "/:userId",
  [authjwt.verifyToken, checkRolesExist, authjwt.isAdmin],
  GetUser
);

router.post(
  "/",
  [
    authjwt.verifyToken,
    authjwt.isAdmin,
    checkRolesExist,
    checkDuplicateUsernameOrEmail,
  ],
  createUser
);

router.put("/:userId", updateUser);
router.delete("/:userId", DeleteUser);

export default router;
