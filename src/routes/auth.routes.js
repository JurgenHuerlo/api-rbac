import { Router } from "express";
import { check } from "express-validator";
const router = Router();

import { signIn, signUp } from "../controllers/auth.controller";
import {
  checkRolesExist,
  checkDuplicateUsernameOrEmail,
} from "../middlewares/verifySignUp";

router.post(
  "/signin",
  [
    check("email", "Invalidad email").isEmail(),
    check("password", "Password can not be empty").not().isEmpty(),
  ],
  signIn
);
router.post(
  "/signup",
  [checkRolesExist, checkDuplicateUsernameOrEmail],
  signUp
);

export default router;
